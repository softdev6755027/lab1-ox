package com.mycompany.lab1_ox;

import java.util.Scanner;

/**
 *
 *
 * @author User
 */
public class Lab1_OX {

    public static void ShowXO(String[] ListXO) {

        System.out.println(ListXO[0] + " " + ListXO[1] + " " + ListXO[2]);
        System.out.println(ListXO[3] + " " + ListXO[4] + " " + ListXO[5]);
        System.out.println(ListXO[6] + " " + ListXO[7] + " " + ListXO[8]);
        System.out.println("");
    }

    public static void checkWinner(String[] arr) {
        if ((arr[0] + arr[1] + arr[2]).equals("XXX")) {
            System.out.println("Congratulations X is the winner !!!");
            System.exit(0);
        } else if ((arr[3] + arr[4] + arr[5]).equals("XXX")) {
            System.out.println("Congratulations X is the winner !!!");
            System.exit(0);
        } else if ((arr[6] + arr[7] + arr[8]).equals("XXX")) {
            System.out.println("Congratulations X is the winner !!!");
            System.exit(0);
        } else if ((arr[0] + arr[3] + arr[6]).equals("XXX")) {
            System.out.println("Congratulations X is the winner !!!");
            System.exit(0);
        } else if ((arr[1] + arr[4] + arr[7]).equals("XXX")) {
            System.out.println("Congratulations X is the winner !!!");
            System.exit(0);
        } else if ((arr[2] + arr[5] + arr[8]).equals("XXX")) {
            System.out.println("Congratulations X is the winner !!!");
            System.exit(0);
        } else if ((arr[0] + arr[4] + arr[8]).equals("XXX")) {
            System.out.println("Congratulations X is the winner !!!");
            System.exit(0);
        } else if ((arr[2] + arr[4] + arr[6]).equals("XXX")) {
            System.out.println("Congratulations X is the winner !!!");
            System.exit(0);
        } else if ((arr[3] + arr[4] + arr[5]).equals("OOO")) {
            System.out.println("Congratulations O is the winner !!!");
            System.exit(0);
        } else if ((arr[6] + arr[7] + arr[8]).equals("OOO")) {
            System.out.println("Congratulations O is the winner !!!");
            System.exit(0);
        } else if ((arr[0] + arr[3] + arr[6]).equals("OOO")) {
            System.out.println("Congratulations O is the winner !!!");
            System.exit(0);
        } else if ((arr[1] + arr[4] + arr[7]).equals("OOO")) {
            System.out.println("Congratulations O is the winner !!!");
            System.exit(0);
        } else if ((arr[2] + arr[5] + arr[8]).equals("OOO")) {
            System.out.println("Congratulations O is the winner !!!");
            System.exit(0);
        } else if ((arr[0] + arr[4] + arr[8]).equals("OOO")) {
            System.out.println("Congratulations O is the winner !!!");
            System.exit(0);
        } else if ((arr[2] + arr[4] + arr[6]).equals("OOO")) {
            System.out.println("Congratulations O is the winner !!!");
            System.exit(0);
        } else if ((arr[0] + arr[1] + arr[2]).equals("OOO")) {
            System.out.println("Congratulations O is the winner !!!");
            System.exit(0);
        }

    }

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        String[] arr = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };
        System.out.println("Welcome to XO");
        ShowXO(arr);

        for (int i = 0; i <= 7; i += 0) {
            System.out.println(i);
            checkWinner(arr);
            System.out.println("X turn Please Input Number");
            int positionX = sc.nextInt();
            int positionO = 0;
            boolean checkX = false;
            boolean checkO = false;
            while (checkX == false) {
                if (arr[positionX - 1].equals("X") || arr[positionX - 1].equals("O")) {
                    System.out.println("X turn Please Input Number");
                    positionX = sc.nextInt();
                } else {
                    checkX = true;
                }
            }
            checkX = false;
            arr[positionX - 1] = "X";
            ShowXO(arr);
            checkWinner(arr);
            i = i + 1;

            System.out.println("O turn Please Input Number");
            positionO = sc.nextInt();

            while (checkO == false) {
                if (arr[positionO - 1].equals("X") || arr[positionO - 1].equals("O")) {
                    System.out.println("O turn Please Input Number");
                    positionO = sc.nextInt();
                } else {
                    checkO = true;
                }
            }
            checkO = false;

            arr[positionO - 1] = "O";
            ShowXO(arr);
            checkWinner(arr);
            i = i + 1;
            System.out.println("");
        }
        System.out.println("Ties, no losers or winners !!");
        sc.close();
    }
}
